#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest
from src.tcmpr.argument_parser import create_parser

__author__ = "Konrad Poreba"
__copyright__ = "Konrad Poreba"
__license__ = "mit"


class CommandLineTestCase:
    """
    Base class for testing CLI, it sets up CLI
    argument parser
    """
    @classmethod
    def setup_class(cls):
        parser = create_parser()
        cls.parser = parser


class TestArgumentParser(CommandLineTestCase):

    def test_with_empty_args(self):
        """
        User passes no args, app should
        fail with SystemExit
        """
        with pytest.raises(SystemExit):
            self.parser.parse_args([])

    def test_pass_dir_with_recursive_flag(self):
        pass

    def test_pass_dir_without_recursive_flag(self):
        """
        Test if error raises if dir was passed
        without proper flag
        """
        # with pytest.raises(SystemExit):
        #     self.parser.parse_args()