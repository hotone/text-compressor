from tcmpr.algorithms.entity.node import Node
from queue import PriorityQueue


class TestNode:
    """Unit tests of node class in entity module"""

    def test_is_leaf(self):
        """test if is_leaf() method works fine"""
        n = Node('AA', 2)
        assert n.is_leaf() is True

    def test_nodes_comparison(self):
        """test if comparison of 2 nodes
           works fine - is lower
        """
        queue = PriorityQueue()
        n_1 = Node('A', 3)
        n_2 = Node('B', 1)

        queue.put((n_1.frequency, n_1))
        queue.put((n_2.frequency, n_2))





