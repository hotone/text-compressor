===============
Getting Started
===============

.. meta::
   :description lang=en: Get started with tcmpr text compression program


tcmpr is a text compression program that uses most popular lossless algorithms.
It can compress using following algorithms:

* Huffman coding
* LZSS
* LZW
* Shannon coding

.. note::

    It's not the fastest program to compression, rather should be considered
    as demonstration of basic lossless algorithms.

Quick start
-----------

To install `tcmpr` use following command (assuming that you have `pip` installed):

.. code-block:: bash

    $ pip install tcmpr

Compress file `filename.txt` with default algorithm (huffman coding):

.. code-block:: bash

    $ tcmpr filename.txt

Decompress file `filename.txt.huffman` with appropriate algorithm based on extension (here `.huffman`):

.. code-block:: bash

    $ tcmpr -d filename.txt.huffman

Parameters
----------

Compress file `filename.txt` with chosen algorithm (here with huffman coding):

.. code-block:: bash

    $ tcmpr -alg=huffman filename.txt

    or

    $ tcmpr --algorithm=huffman filename.txt

Help
----

Help can be invoked in following way:

.. code-block::

    $ tcmpr -h
    usage: tcmpr [-h] [-c] [-d] [-alg {huffman,lzss,lzw,shannon}] [-r] [--version]
                 [-v] [-vv]
                 filename

    tcmpr program to compress or decompress text data

    positional arguments:
      filename              Name of file which you would like to
                            compress/decompress

    optional arguments:
      -h, --help            show this help message and exit
      -c, --compress        set if you want to compress file
      -d, --decompress      set if you want to decompress file
      -alg {huffman,lzss,lzw,shannon}, --algorithm {huffman,lzss,lzw,shannon}
                            choose algorithm which you would like to use in
                            compression
      -r, --recursive
      --version             show program's version number and exit
      -v, --verbose         set loglevel to INFO
      -vv, --very-verbose   set loglevel to DEBUG

