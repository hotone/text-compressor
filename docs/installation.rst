============
Installation
============

Requirements
============

The installation of tcmpr only requires a recent version of `pip`_ and python 3.

.. note::

    Program is distributed as binary wheel package, so all dependencies are included.

Installation
============

Make sure you have ``pip`` installed, then simply type::

   $ pip install tcmpr

The most recent version can be
installed with::

   $ pip install --upgrade tcmpr

Using ``pip`` also has the advantage that all requirements are automatically
installed.

Uninstall
=========
If you would like to uninstall tcmpr use following command::

   $ pip uninstall tcmpr

.. _pip: https://pypi.org/project/pip/

