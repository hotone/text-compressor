=====
tcmpr
=====

This is the documentation of **tcmpr**.

Program compress and decompress text files, using chosen
algorithm. It can run on files as well as
in recursive way on directory with text files.
Implemented algorithms are:

- Huffmann (`Huffman coding`__)
- LZSS ( `Lempel–Ziv–Storer–Szymanski`__)
- LZW (`Lempel–Ziv–Welch`__)
- Shannon (`Shannon coding`__)


__ HUFFMAN_
__ LZSS_
__ LZW_
__ SHANNON_
.. _HUFFMAN: https://en.wikipedia.org/wiki/Huffman_coding
.. _LZSS: https://en.wikipedia.org/wiki/Lempel%E2%80%93Ziv%E2%80%93Storer%E2%80%93Szymanski
.. _LZW: https://en.wikipedia.org/wiki/Lempel%E2%80%93Ziv%E2%80%93Welch
.. _SHANNON: https://en.wikipedia.org/wiki/Shannon_coding

Contents
========

.. toctree::
   :maxdepth: 2

   Installation <installation>
   Getting Started <getting_started>
   License <license>
   Authors <authors>
   Changelog <changelog>
   Module Reference <api/modules>



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. _toctree: http://www.sphinx-doc.org/en/master/usage/restructuredtext/directives.html
.. _reStructuredText: http://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html
.. _references: http://www.sphinx-doc.org/en/stable/markup/inline.html
.. _Python domain syntax: http://sphinx-doc.org/domains.html#the-python-domain
.. _Sphinx: http://www.sphinx-doc.org/
.. _Python: http://docs.python.org/
.. _Numpy: http://docs.scipy.org/doc/numpy
.. _SciPy: http://docs.scipy.org/doc/scipy/reference/
.. _matplotlib: https://matplotlib.org/contents.html#
.. _Pandas: http://pandas.pydata.org/pandas-docs/stable
.. _Scikit-Learn: http://scikit-learn.org/stable
.. _autodoc: http://www.sphinx-doc.org/en/stable/ext/autodoc.html
.. _Google style: https://github.com/google/styleguide/blob/gh-pages/pyguide.md#38-comments-and-docstrings
.. _NumPy style: https://numpydoc.readthedocs.io/en/latest/format.html
.. _classical style: http://www.sphinx-doc.org/en/stable/domains.html#info-field-lists
